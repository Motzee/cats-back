<?php

namespace Database\Seeders;

use App\Models\Catbreed;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CatbreedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Catbreed::factory(10)->create();
    }
}
