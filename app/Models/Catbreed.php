<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Catbreed extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'catbreeds';

    /**
     * column in DB used for URLs
     */
    public function getRouteKeyName()
    {
        return "slug";
    }

    protected $fillable = [
        'slug',
        'name',
        'picture',
        'health',
        'description'
    ] ;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at'            => 'datetime',
        'updated_at'            => 'datetime',
        'deleted_at'            => 'datetime'
    ];
}
