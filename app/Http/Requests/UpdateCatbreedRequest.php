<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCatbreedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "name" => "required|string|max:255",
            "picture" => "string|max:255|nullable",
            "health" => "string|nullable",
            "description" => "string|nullable",
        ];
    }

    /**
     * Customize error messages
     *
     * @return array
     */
    public function messages() {
        return [
            'name.required'           => 'Ce champ est requis',
        ];
    }
}
