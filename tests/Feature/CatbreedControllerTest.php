<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Catbreed;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CatbreedControllerTest extends TestCase
{
    //réinitialisation de la BDD
    use RefreshDatabase ;

    /**
     * Un user non authentifié ne doit pouvoir poster des entrées
     */
    public function test_postCatbreedApi_withoutLogin_redirectToLogin(): void
    {
        $response = $this->post(route('catbreeds.store'), [
            "name"  => "Laperm",
            "picture"   => "breed1.png",
            "health"    => "lignée génétique fiable et rustique de par les origines de cette race.",
            "description"   => "Un chat tout bouclé, du à la présence d’une mutation génétique à allèle dominante. Cet allèle est différent des autres races Rex"
        ]);

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }


    /**
     * Un user authentifié doit pouvoir poster des entrées
     */
    public function test_postCatbreedApi_withLogin_success(): void
    {
        $user = User::factory()->create([
            'email' => 'test@example.com',
            'password' => Hash::make('password'),
        ]);

        $this->actingAs($user);

        $response = $this->post(route('catbreeds.store'), [
            "name"  => "Laperm",
            "picture"   => "breed1.png",
            "health"    => "lignée génétique fiable et rustique de par les origines de cette race.",
            "description"   => "Un chat tout bouclé, du à la présence d’une mutation génétique à allèle dominante. Cet allèle est différent des autres races Rex"
        ]);

        $response->assertStatus(201);
        $this->assertCount(1, Catbreed::all()) ;
    }
}
