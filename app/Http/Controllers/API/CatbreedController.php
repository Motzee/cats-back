<?php

namespace App\Http\Controllers\API;

use App\Models\Catbreed;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Resources\CatbreedResource;
use App\Http\Requests\StoreCatbreedRequest;
use App\Http\Requests\UpdateCatbreedRequest;

class CatbreedController extends Controller
{
/**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //récupération d’une liste résumant les entrées non deleted ; pas de pagination
        $catbreeds = Catbreed::select(['slug', 'name', 'picture'])->get();

        return CatbreedResource::collection($catbreeds);
        //return response()->json($catbreeds);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCatbreedRequest $request)
    {
        $validated = $request->validated();

        $validated['slug'] = Str::slug(trim($validated['name']));
        $nCatbreed = Catbreed::create($validated);

        return response()->json($nCatbreed, 201);
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\Catbreed  $catbreed
     * @return \Illuminate\Http\Response
     */
    public function show(Catbreed $catbreed)
    {
        return new CatbreedResource($catbreed);
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Catbreed  $catbreed
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCatbreedRequest $request, Catbreed $catbreed)
    {
        $validated = $request->validated();
        if (isset($validated['picture'])) {
            $urlArray = explode("/", $validated['picture']);
            $validated['picture'] = end($urlArray);
        }

        $catbreed->update($validated);

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Models\Catbreed  $catbreed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Catbreed $catbreed)
    {
        $catbreed->delete();

        return response()->json();
    }
}
