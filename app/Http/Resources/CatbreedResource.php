<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CatbreedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray(Request $request): array
    {
        //white-liste des champs à renvoyer
        $fields = [
            "slug"  => $this->slug,
            "name"  => $this->name,
            "picture"  =>config('app.url').config('app.catbreed_picture_folder'). $this->picture,
            "health"  => $this->health,
            "description"  => $this->description
        ] ; 

        return $fields ;
    }
}
