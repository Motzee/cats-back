<?php

namespace Tests\Feature;

use App\Models\Catbreed;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CatbreedTest extends TestCase
{
    //réinitialisation de la BDD
    use RefreshDatabase ;

    /**
     * A basic feature test example (have to be prfixed by test_).
     */
    public function test_store_a_catbreed(): void
    {
        Catbreed::create([
            "name"  => "Laperm",
            "slug"  => "laperm",
            "picture"   => "breed1.png",
            "health"    => "lignée génétique fiable et rustique de par les origines de cette race.",
            "description"   => "Un chat tout bouclé, du à la présence d’une mutation génétique à allèle dominante. Cet allèle est différent des autres races Rex"
        ]) ;

        $this->assertCount(1, Catbreed::all()) ;
    }
}
