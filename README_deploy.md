# Requirements
- php 8.2
- mysql 8.0
- composer v2
- node 18

# Deploy

WARNING ! This project need to be on the same domain as its API cats-back (https://gitlab.com/Motzee/cats-back)

1. Git clone the project
2. Create a database (utf_8_unicode_ci)
3. Copy the .env.examble -> .env and complete data
4. At the root of the project, make these commands :

``` 
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
``` 

5. Run the dev server :
```
php artisan serve
```

6. Test url `/api/catbreeds` : you should obtain a json list

# Tests  
/ ! \ never run tests in Production (too many risks to forgot to change the DB)
1. create db cats_unittests
2. duplicate .env -> .env.testing and change the DB infos
3. php artisan cache:clear
4. php artisan test (all must be green)