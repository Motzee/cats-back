<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Catbreed>
 */
class CatbreedFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->unique()->name() ;
        
        // (pictures of minimalist cats from Suryadi (vecteezy.com))

        return [
            'slug'  => Str::slug(trim($name)),
            'name'  => $name,
            'picture'   => sprintf('breed%1$d.png', fake()->numberBetween(1, 6)),
            'health'    => fake()->text($maxNbChars = 250),
            'description'   => fake()->text($maxNbChars = 3000)
        ];
    }
}
